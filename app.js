var express  = require('express');
var app = express();
var http = require('http').Server(app);
var io   = require('socket.io')(http);
var striptags = require('striptags');
var xss = require("xss");
app.use(express.static(__dirname + '/public'));
var md5 = require('md5');
var mysql = require('mysql')
var moment = require('moment');
var emptyCheck = require('is-empty');
const Entities = require('html-entities').XmlEntities;
const entities = new Entities();
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'akhil123',
    database : 'chat',
    multipleStatements: true,
    charset : 'utf8mb4'
});

connection.connect()

app.get('/', function(req, res) {
    res.sendFile(__dirname+'/index.html');
});
app.post('/chat', function(req, res) {
    console.log('-------------------------------------[1]');
    res.sendFile(__dirname+'/chat.html');
});
var users = [];
let messages = [];
var people={};
var error = '';

io.on('connection', function(socket) {
    console.log('A user connected');
    socket.emit('connected', {connected:'Yay!'});
    // socket.on('testmessage', function(data, callback){
    //     console.log('Socket (server-side): received message:', data);
    //     var responseData = { string1:'I like ', string2: 'bananas ', string3:' dude!' };
    //     //console.log('connection data:', evData);
    //     callback(responseData);
    // });
    socket.on('setUsername',function(data){
        let name = data.name;
        let password = data.password;
        getUserInfo(name, function(result){
            if(!emptyCheck(result)){
                socket.emit('userExist');
            }else{
                let userDetail = {};
                userDetail.socketId = socket.id;
                userDetail.name = name;
                userDetail.password = password;

                createUser(userDetail, function(userArr){ 
                    if(userArr){
                        console.log("insert user success");
                        console.log(userArr);
                        
                        socket.emit('userCreated',userArr);
                        getUsers( function(results){
                            io.sockets.emit('userlist',results);
                        });
                    }else{
                        error = "create user failed"
                        console.log("insert user failed");                        
                        socket.emit('error',error);
                    }
                });
            }
        });
       
        
    });
    socket.on('login',function(data){
        let name = data.name;
        let password = data.password;
        loginUser(data, function(result){           
            if(!emptyCheck(result)){
                let userDetail = {};
                userDetail.socket_id = socket.id;
                userDetail.name = name;
                userDetail.id = result.id;
                console.log("login user success");
                console.log(result);
                joinAllRooms(result.id);
                updateSocketId(result.id, socket.id);
                socket.emit('userCreated',userDetail);
                getUsers( function(results){
                    io.sockets.emit('userlist',results);
                });
                
           }else{                
                console.log('-------------------------------------[1]');
                socket.emit('userNotExist');
                console.log('-------------------------------------[2]');
            }
        });      
        
    })
    socket.on('getGroupList',function(){
        let socketId = socket.id;
        getUserId(socketId, function(fromUserId){ 
            getGroupList (fromUserId, function(result){
                io.sockets.in(socketId).emit('groupList',result);
            });
        });
    })
    socket.on('loginByRefresh',function(data){
        let userId   = data.user_id;
        getUserDetails(userId, function (result) {
            if(result){
                let data          = {};
                    data.name     = result.name;
                    data.password = result.password;
                    console.log('data = ',data );  
                socket.emit('loginAgain',data);
            }
            console.log('result = ',result );
        });        
    })
    socket.on('createGroup',function(data){
        createGroup(data, function(result){ 
            if(result){   
                if(typeof result.group_id !== 'undefined'  && result.group_id !== ''){
                    result.members.forEach((element)=>{
                        getSocketId(element, function(toSocketId){
                            io.sockets.in(toSocketId).emit('addToGroup',{group_id:result.group_id, room_name:data.group_name});
                        });
                        let addMemeberArr = {};
                        addMemeberArr.type = 'group'
                        addMemeberArr.from = element;
                        addMemeberArr.to = result.group_id;
                        addMemeberArr.group_type = 'add';    
                        addMemeberArr.message = 'groupCreated';    

                        console.log('-------------------------------------[3]');
                        saveMessage(addMemeberArr, function(results) {
                            console.log('Add User to group  successfully');
                        }) ;
                    });
                    socket.emit('group_created_success', 
                    {id:result.group_id, name:data.group_name }
                    );
                }else{console.log('-------------------------------------[2]');
                    socket.emit('error',error);
                }
            }else{
                socket.emit('error',error);
            }
        });
    });
    socket.on('groupJoin',function(data){
        socket.join(`room_${data.group_id}`);
        socket.emit('getGroupList');

    });
    function createGroup(data,callback){
        let group_name = entities.encode(data.group_name);

        var sql = `INSERT INTO room set ?`;  
        connection.query(sql,{room_name :group_name}, function(err, results){
            if (err){ 
                console.log( "Error in database connection");
                throw err;
            } 
            
            let insertedId = results.insertId;
            var query2 = "INSERT INTO room_members (room_id, user_id) VALUES ?";
            var values = [];          
            for (var key in data.members) {
                let tempArr = [];
                tempArr.push(insertedId);
                tempArr.push(data.members[key]);
                values.push(tempArr);
            }
            connection.query(query2, [values], function(err) {
                if (err) throw err;
            });
            let resultArr = {};
            resultArr.group_id = insertedId;
            resultArr.members = data.members;    
            return callback(resultArr);  
            
        });
    }  
    function getUsers(callback){
        var sql = `SELECT * from users`;  
        connection.query(sql, function(err, results){
            if (err){ 
                console.log( "Error in database connection");
                throw err;
            } 
            return callback(results);  
            
        });
    }   
   
    function joinAllRooms(userId){
        var sql = `SELECT  r.id from room_members as m inner join room as r on r.id = m.room_id where m.user_id = ?  `;
        connection.query(sql,[userId], function(err, results){
            if (err){ 
                console.log( "Error in database connection");
                throw err;
            } 
            if( typeof results !== 'undefined' && results.length>0){
                results.forEach(element => {
                    socket.join(`room_${element.id}`);
                });
            }
            return (true);  
            
        });
    }  
    function getGroupList (userId, callback){
        var sql = `SELECT r.* FROM room_members as m inner JOIN room as r ON r.id = m.room_id where m.user_id ='${userId}'`;  
        connection.query(sql, function(err, results){
            if (err){ 
                console.log( "Error in database connection");
                throw err;
            } 
            return callback(results);  
        });
    }  
    function loginUser(obj, callback){
        let data = {};
        var sql = `SELECT * from users where name = '${obj.name}' and password='${obj.password}'`;  
        connection.query(sql, function(err, results){
            if (err){ 
                console.log( "Error in database connection");
                throw err;
            } 
            if(results.length >=1 ){
                console.log('user Exist');
                data = results[0];
                return callback(data);  
            }else{
                console.log('user not exist');
                return callback(data);  
            }                     
        });
    };
    function getUserInfo(name, callback){
        let data = {};
        var sql = `SELECT * from users where name = ?`;  
        connection.query(sql, [name],function(err, results){
            if (err){ 
                console.log( "Error in database connection");
                throw err;
            } 
            if(results.length >=1 ){
                console.log('user Exist');
                data = results[0];
                return callback(data);  
            }else{
                console.log('user not exist');
                return callback(data);  
            }                     
        });
    };
    function getUserDetails(id, callback){
        let data = {};
        var sql = `SELECT * from users where id = '${id}'`;  
        connection.query(sql, function(err, results){
            if (err){ 
                console.log( "Error in database connection");
                throw err;
            } 
            if(results.length >=1 ){
                console.log('user Exist');
                data = results[0];
                return callback(data);  
            }else{
                console.log('user not exist');
                return callback(data);  
            }                     
        });
    };
    function createUser(data, callback){
        let name     = entities.encode(data.name);
        let password = entities.encode(data.password);
        let obj = {
            name: name,
            password: password,
            socket_id:data.socketId
        };
        var sql = `INSERT INTO users SET ?`;         
        connection.query(sql, obj, function(err, results){
            if (err){ 
                console.log( "Error in database connection - create user");
                throw err;
            }   
            return callback({id:results.insertId,name:data.name, password:data.password});   
        });
    };
    function updateSocketId(userId, socketId){
        var sql = `UPDATE users SET socket_id = '${socketId}' WHERE id = ${userId}`;        
        connection.query(sql, function(err, results){
            if (err){ 
                console.log( "Error in database connection - create user");
                throw err;
            }   
            console.log(results);
            console.log('-------------------------------------[2]');
            return (true);   
        });
    };
    function getSocketId(id, callback){
        let data = {};
        // console.log(obj);
        var sql = `SELECT socket_id from users where id = '${id}'`;  
        connection.query(sql, function(err, results){
            if (err){ 
                console.log( "Error in database connection");
                throw err;
            } 
            if(results.length ==1 ){
                data = results[0];
                return callback(data.socket_id);  
            }else{
                console.log('Socket Id getting failed');
                return callback(data);  
            }                     
        });
    };
    function getUserId(socketId, callback){
        let data = {};
        let userId = '';
        var sql = `SELECT id from users where socket_id = '${socketId}'`;  
        connection.query(sql, function(err, results){
            if (err){ 
                console.log( "Error in database connection -1");
                throw err;
            }            
            console.log(results);
            data = results[0];
            console.log(typeof data);           
            if(data !== '' && typeof data !== 'undefined' && typeof data.id !== 'undefined' && data.id !== ""){
                userId = data.id;
            }
            return callback(userId);              
        });
    };
    socket.on('sendPrivateMessage',function(data){
        console.log(data);
        io.sockets.in(data.email).emit('new_msg', {msg: data.msg,from:data.from});
        
    });
    socket.on('startChat', function(data){
        // console.log('----------------Start chat To perosn ---------------------id:',data);
        // console.log(socket.id);
        let socketId = socket.id;        
        getUserId(socketId, function(fromUserId){ 
            let objArr      = {};
                objArr.type = data.type;
                objArr.from = fromUserId ;
                objArr.to   = data.to;            
            if(fromUserId){
                getMessages(objArr,function(messages){
                    let resObj          = {};
                        resObj.messages = messages;
                        resObj.from     = objArr.from;
                        resObj.to       = objArr.to;
                        resObj.type     = data.type;

                    io.sockets.in(socketId).emit("messages", resObj);  
                });                
            }
        });
    });
    function getMessages(data, callback){
        if(data.type == 'personal'){
            var sql=`SELECT m.* , u.name as from_name, users.name as to_name 
            FROM messages as m LEFT JOIN users as u ON u.id = m.from_id 
            LEFT JOIN users ON users.id = m.to_id
            where m.type = '${data.type}' and ( m.from_id='${data.from}' or m.from_id='${data.to}') and ( m.to_id='${data.from}' or m.to_id='${data.to}')`;
          
        }else{
            var sql=`SELECT m.* , u.name as from_name, room.room_name as to_name 
            FROM messages as m LEFT JOIN users as u ON u.id = m.from_id 
            LEFT JOIN room ON room.id = m.to_id
            
            where m.type = '${data.type}' and  m.to_id='${data.to}'`;
      
        }
        connection.query(sql, function(err, results){
            if (err){ 
                console.log( "Error in database connection");
                throw err;
            } 
            return callback(results);  
        });
    };
    function saveMessage(data, callback){console.log('data = ',data );
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var currentDate = date+' '+time;
      
        let groupType = "";
        if (typeof data.group_type !== 'undefined' &&  data.group_type !== ''){
            groupType =  data.group_type
        }

        let newMsg =  (data.message);
        console.log('newMsg = ',newMsg );
     
        let obj = {
            type: data.type,
            from_id: data.from,
            to_id:data.to,
            message:newMsg,
            group_type:groupType,
            date:currentDate,

        };
      
        var sql=`INSERT INTO messages SET ?`;
        connection.query(sql, obj, function(err, results){
            if (err){ 
                console.log( "Error in database connection -savemessage");
                throw err;
            } 
            return callback(true);  
        });
    };
    socket.on('sendMessage', function(data){
        let socketId = socket.id;  
        getUserId(socketId, function(userId){
            if(!userId){
                console.log('----------------user expired---------------------[1]');
                io.sockets.in(socketId).emit("session expired"); 
            }
            saveMessage(data, function (result) {
                let objArr      = {};
                objArr.type = data.type;
                objArr.from = data.from ;
                objArr.to   = data.to;
                if(data.type == 'group'){
                    getMessages(objArr,function(messages){
                        let resObj          = {};
                            resObj.messages = messages;
                            resObj.from     = objArr.from;
                            resObj.to       = objArr.to;
                            resObj.to_name  = data.name;
                            resObj.type     = 'group';
                        console.log('-------------------------------------[group]');
                        console.log("room_"+data.to);
                        io.sockets.in("room_"+data.to).emit('messages', resObj);
                    });
                }else{
                    getSocketId(data.to , function(toSocketId){
                        
                        getMessages(objArr,function(messages){
                            console.log("messages");
                            let resObj          = {};
                                resObj.messages = messages;
                                resObj.from     = objArr.from;
                                resObj.to       = objArr.to;
                                resObj.to_name  = data.name;
                            resObj.type = 'personal';
                            io.sockets.in(toSocketId).emit("messages", resObj); 
                            io.sockets.in(socketId).emit("messages", resObj); 
                            
                        });     
                    })
                }
            })          
        })
    })
    // when the client emits 'typing', we broadcast it to others
    socket.on('typing', (data) => {
        getSocketId(data.toId , function(toSocketId){
            io.sockets.in(toSocketId).emit("typing", {fromId:data.fromId}); 
        });
    });
    
    // // when the client emits 'stop typing', we broadcast it to others
    socket.on('stop typing', (data) => {
        getSocketId(data.toId , function(toSocketId){
            io.sockets.in(toSocketId).emit("stop typing", {fromId:data.fromId}); 
        });
    });   
    socket.on('disconnect', () => {
        console.log('Disconnect user');
    });

});


http.listen(80, function() {
    console.log('listening on *:3000');
});

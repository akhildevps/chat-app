-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 08, 2019 at 12:32 PM
-- Server version: 5.7.24
-- PHP Version: 7.1.26-1+0~20190113101856.12+jessie~1.gbp7077bb

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `chat`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
`id` int(11) NOT NULL,
  `type` enum('personal','group') CHARACTER SET latin1 NOT NULL,
  `from_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `to_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `message` text NOT NULL,
  `group_type` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `type`, `from_id`, `to_id`, `message`, `group_type`, `date`) VALUES
(1, 'personal', '1', '2', 'hi dear', '', '2019-03-08 12:28:52'),
(2, 'personal', '2', '1', 'hi akhl iam ram', '', '2019-03-08 12:29:07'),
(3, 'personal', '2', '1', '<img class="img 123456" src="images//blank.gif" style="display:inline-block;width:25px;height:25px;background:url(''images//emoji_spritesheet_0.png'') -300px 0px no-repeat;background-size:675px 175px;" alt=":stuck_out_tongue_closed_eyes:"><img class="img 123456" src="images//blank.gif" style="display:inline-block;width:25px;height:25px;background:url(''images//emoji_spritesheet_0.png'') -300px 0px no-repeat;background-size:675px 175px;" alt=":stuck_out_tongue_closed_eyes:"><img class="img 123456" src="images//blank.gif" style="display:inline-block;width:25px;height:25px;background:url(''images//emoji_spritesheet_0.png'') -300px 0px no-repeat;background-size:675px 175px;" alt=":stuck_out_tongue_closed_eyes:"><img class="img 123456" src="images//blank.gif" style="display:inline-block;width:25px;height:25px;background:url(''images//emoji_spritesheet_0.png'') 0px 0px no-repeat;background-size:675px 175px;" alt=":smile:"><img class="img 123456" src="images//blank.gif" style="display:inline-block;width:25px;height:25px;background:url(''images//emoji_spritesheet_0.png'') 0px 0px no-repeat;background-size:675px 175px;" alt=":smile:">', '', '2019-03-08 12:29:12'),
(4, 'personal', '1', '2', 'hi&nbsp;<img class="img 123456" src="images//blank.gif" style="display:inline-block;width:25px;height:25px;background:url(''images//emoji_spritesheet_0.png'') -425px 0px no-repeat;background-size:675px 175px;" alt=":relieved:"><img class="img 123456" src="images//blank.gif" style="display:inline-block;width:25px;height:25px;background:url(''images//emoji_spritesheet_0.png'') -425px 0px no-repeat;background-size:675px 175px;" alt=":relieved:"><img class="img 123456" src="images//blank.gif" style="display:inline-block;width:25px;height:25px;background:url(''images//emoji_spritesheet_0.png'') -525px 0px no-repeat;background-size:675px 175px;" alt=":cry:"><img class="img 123456" src="images//blank.gif" style="display:inline-block;width:25px;height:25px;background:url(''images//emoji_spritesheet_0.png'') -525px 0px no-repeat;background-size:675px 175px;" alt=":cry:">', '', '2019-03-08 12:29:37'),
(5, 'personal', '1', '2', '<br><br><br><br><br>sdfasfasdfasf', '', '2019-03-08 12:29:46');

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE IF NOT EXISTS `room` (
`id` int(11) NOT NULL,
  `room_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `room_members`
--

CREATE TABLE IF NOT EXISTS `room_members` (
`id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `socket_id` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `socket_id`) VALUES
(1, 'akhil', '123', 'AkERR4a_qTsyFQl-AAAN'),
(2, 'ram', '123', 'dyYP5t6gawf4warYAAAO');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_members`
--
ALTER TABLE `room_members`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `room_members`
--
ALTER TABLE `room_members`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

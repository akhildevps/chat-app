var startChat = '';
var message   = {};
let chatType  = "personal";
let userArr   = [];
$(function(){  
    // var str = `Lorem ipsum <img class="img">text 1</img> Lorem ipsum  <img class="img">text 2</img>`;
    // var str = `<div class="sss"><img class="img 123456" src="images//blank.gif"  style="display:inline-block;width:25px;height:25px;background:url('images//emoji_spritesheet_0.png') -25px 0px no-repeat;background-size:675px 175px;" alt=":smiley:">qq</img>
    // </div>`;
    // let count = 0;
    // let str2 = str; 
    // var str3 = '';
    // let obj = {};  
    // let replaceName = '';
    
    // const stringVal = 'SKMG_';
    // str.replace(/<img class="img(.*?)<\/img>/g, function(match, g1) {
    //     console.log('g1 = ',g1 );
    //     console.log('match = ',match );
    //     replaceName = stringVal+count;
    //     str2 = str2.replace(match,replaceName);
    //     obj[replaceName] = match;
    //     count++;        
    // });
    // for (var key in obj) {
    //     str2 = str2.replace(key, obj[key]);
    // }

    var TYPING_TIMER_LENGTH = 1200; // ms
    var socket = io();
    var typing = false;
    $('.js-example-basic-multiple').select2();
    checkLogin();
    $('#setUsername').on('click', function () {
        setUsername();
    }); 
    function setUsername(){
        let name     = $.trim($('#name').val());
        let password = $.trim($('#password').val());
        var errFlag = 0;   
		$('div').removeClass('has-error');
		$('.help-block').hide();	
		if (name == '' ) {      
			$('#name').parent('div').addClass('has-error');
			$('#name').siblings('.help-block').show();
			errFlag = 1;
        }   
        if (password == '' ) {      
			$('#password').parent('div').addClass('has-error');
			$('#password').siblings('.help-block').show();
			errFlag = 1;
        }   
        if(errFlag == 0){
            socket.emit('setUsername', {name:name, password:password});
        }
    } 
    function checkLogin(){
        if (typeof sessionStorage.user_id !=='undefined' && sessionStorage.user_id !== '') {
            console.log('sessionStorage = ',sessionStorage );
            let data  = {};
            data.user_id = sessionStorage.user_id;
            socket.emit('loginByRefresh',data);
        } 
    }
    $('#login').on('click', function () {
        login();
    }); 
    $('.cancelRegistration').on('click', function () {
        location.reload();

    }); 
   socket.on("loginAgain", function (data){console.log('-------------------------------------[1]');
    socket.emit('login', {name:data.name, password:data.password});
   })
    $('.button_add').on('click', function () {
        $('.success_group_create').addClass("hide");
        $('.group_div').removeClass("hide");
        $('.messages').addClass("hide");
        $('.group_create_form').removeClass("hide");
        $('.chat_empty').addClass('hide');
        var str = '';
        let userId =  $('#usernameId').val();
        userArr.forEach((element)=>{
            if(userId != element.id){
                str += `<option value="${element.id}">${element.name}</option>`;
            }
        });
        $('.members_list').html(str);
    }); 
    $('#cancel_member_btn').on('click', function () {
        $( ".group_chat" ).trigger( "click");

    }); 
    $('#add_member_btn').on('click', function () {
        var errFlag = 0;   
		$('div').removeClass('has-error');
		$('.help-block').hide();	
		if ($.trim($('#group_name').val()) == '' ) {      
			$('#group_name').parent('div').addClass('has-error');
			$('#group_name').siblings('.help-block').show();
			errFlag = 1;
        }   
        if(errFlag == 0){
            let data       = {};
            let membersArr = [];
            let userId     = $('#usernameId').val();
            membersArr.push(userId)
            let selectedMembers = $('.js-example-basic-multiple').val();
            let groupName       = $('#group_name').val();
            
            if(selectedMembers){
                membersArr = membersArr.concat(selectedMembers);
            }
            data.group_name = groupName;
            data.members    = membersArr;
            socket.emit('createGroup', data);
        }
    })
    socket.on('group_created_success',(data)=>{
        $( ".group_chat" ).trigger( "click");
        $("#to_id").val(data.id);
        $('.group_create_form').addClass("hide");
        $('.success_group_create').removeClass("hide");
    })
    function login(){
        let name     = $('#loginusername').val();
        let password = $('#loginpassword').val();
        socket.emit('login', {name:name, password:password});
    }
    socket.on('userExist',function(data){
        
        $('.error-div').html( ` <div class="alert alert-danger fade in alert-class  ">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Error!</strong> User name Already Exist  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;       
        </div>`);     
    });
    socket.on('userNotExist',function(){
        $('.error-div').html( ` <div class="alert alert-danger fade in alert-class  ">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Error!</strong> username or password incorrect  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;       
        </div>`);    
    })
    socket.on('error',function(){
        alert('Error');
    })
    socket.on('userCreated', function(data){
        sessionStorage.user_id   = data.id;
        sessionStorage.user_name = data.name;
        let name =  data.name;       
        $('#usernameval').val(name);
        $('#user-namearea').append(name);
        $('#usernameId').val(data.id);
        $('.main').addClass('hide');
        $('.chat-div').removeClass('hide');   
    });
    $('.single_chat').on('click',function(){
        $('.button_add').addClass('hide');
        $('.single_chat').addClass('active');
        $('.single_chat_div').removeClass('hide');
        $('.group_chat').removeClass('active');
        $('.group_chat_div').addClass('hide');
        $('.group_div').addClass("hide");
        $('.chat_empty').addClass("hide");
        $('.chat_empty_list').addClass('hide');

        chatType = 'personal';   
        $( "ul li.personal_li:first" ).trigger( "click" );
    });
    
    $('.group_chat').on('click',function(){
        $('.button_add').removeClass('hide');
        $('.group_chat').addClass('active');
        $('.group_chat_div').removeClass('hide');
        $('.single_chat').removeClass('active');
        $('.single_chat_div').addClass('hide');
        $('.group_div').addClass('hide');
        
        chatType = 'group';
        socket.emit('getGroupList');
        $( "ul li.group_li:first" ).trigger( "click" );
    });
    $('#sendMessage').on('click', function () {
        let to_id          = $("#to_id").val();
        let spanClass      = 'span_message_'+to_id ;
        let spanClassGroup = 'span_message_group_'+to_id ;


        $('#'+spanClass).addClass("hide");   
        $('#'+spanClassGroup).addClass("hide");   
        sendMessage();
    });
     $('#chat_image').on('click', function () {
         alert('');
     });
    socket.on('userlist', function(data){
        $('.chat_empty_list').addClass('hide');

        let str = '';
        let userId =  $('#usernameId').val();
        let toId = $('#to_id').val();
        userArr = data;
        if(typeof data !== 'undefined' && data.length >0 ){
            data.forEach(element => {
                if(userId != element.id){
                    str += `<li class ="personal_li personal_${element.id}" onClick="startChat('${element.id}', '${element.name}','personal')">
                    <a href="#"  class="element_list">${element.name} 
                    <span class="badge pull-right hide"  id ="span_message_${element.id}">*</span>
                    </a></li>`;
                }
            });
            $('.chat_empty').addClass('hide');
            $('.messages').removeClass('hide');
        }else{
            $('.messages').addClass('hide');
            $('.chat_empty').removeClass('hide');
        }
        $('.chat_users').html(str);
        if(typeof toId === 'undefined'){
            $( "ul li.personal_li:first" ).trigger( "click" );
        }else{
            $('.personal_'+toId).addClass('active_li');
        }
    });
    socket.on('groupList', function(data){
        let str = '';
        let userId =  $('#usernameId').val();
        let toId = $('#to_id').val();
        $('.chat_empty').addClass('hide');
        $('.chat_empty_list').addClass('hide');
        if(typeof data !== 'undefined' && data.length >0 ){
            data.forEach(element => {
                str += `<li class ="group_li group_${element.id}" onClick="startChat('${element.id}','${element.room_name}','group')">
                <a href="#"  class="element_list">${element.room_name} 
                <span class="badge pull-right hide"  id ="span_message_group_${element.id}">*</span>
                </a></li>`;
            });
            $('.chat_empty').addClass('hide');
            $('.messages').removeClass('hide');
        }else{
            $('.messages').addClass('hide');
            $('.chat_empty').removeClass('hide');
            $('.chat_empty_list').removeClass('hide');
        }
        $('.chat_groups').html(str);
        if(toId == ''){
            $( "ul li.group_li:first" ).trigger( "click" );
        }else{
            $( "ul li.group_"+toId ).trigger( "click" );
        }
    });
    
    startChat = function (to, name,type)
    {     
        $('.group_div').addClass("hide");
        $('.messages').removeClass('hide');
        if(type == 'personal'){
            $('li').removeClass('active_li');            
            $('.personal_'+to).addClass('active_li');
            let spanClass = 'span_message_'+to ;        
            $('#'+spanClass).addClass("hide");    
        }else{
            $('li').removeClass('active_li');
            $('.group_'+to).addClass('active_li');
            let spanClass = 'span_message_group_'+to ;        
            $('#'+spanClass).addClass("hide");   
        }
        let str = '';
        str += ` <div class="chat-heading">
            <div class="pad-5">
                <div class="row">
                    <div class="col-md-1  " >
                        <img src="images/user02.png" alt="Avatar" style="width:45px;">
                    </div>
                    <div class="col-md-7">
                        <h4 class="chat_user_heading">${name} </h4>

                        <input id="to_name" class="hide" value="${name}">
                        <input id="to_id" class="hide" value="${to}">
                    </div> 
                      <div class="col-md-5 hide typing_div_${to}">Typing...
                      </div>
                </div>
            </div>
        </div> 
        `;
        $('.chat_name').html(str);
       
        socket.emit('startChat',{to:to,name:name,type:type});
    } 
    socket.on("session expired",function(){
        alert('Session Expired');
        sessionStorage.clear();
        socket.emit('disconnect');
        
    });
    socket.on("addToGroup",function(data){
        socket.emit('groupJoin',{group_id:data.group_id});
        let str = '';
        str += `<li class ="group_li group_${data.group_id}" onClick="startChat('${data.group_id}','${data.room_name}','group')">
            <a href="#"  class="element_list">${data.room_name} 
             <span class="badge pull-right hide "  id ="span_message_group_${data.group_id}">*</span>
             </a></li>`;
             let chatId = $('#to_id').val();
             $('.chat_groups').append(str);
             if(data.group_id !== chatId){
                $('#span_message_group_'+data.group_id).removeClass('hide');
            }
        });

    socket.on("messages",function(data){
        let str = '', groupAdmin='';
        let fromId = data.from;
        let toId = data.to;
        let messages = data.messages;
        let currentUserId = $('#usernameId').val();
        let chatId = $('#to_id').val();
        let spanId = toId;
        if(currentUserId == toId && data.type == 'personal'){
            spanId= fromId;
        }
        if(messages.length>0 && fromId != currentUserId){
             if(spanId !== chatId){              
                if(messages[0].type == 'group'){
                    let spanClass = 'span_message_group_'+spanId ;        
                    $('#'+spanClass).removeClass("hide");  
                }else {
                    let spanClass = 'span_message_'+spanId ;        
                    $('#'+spanClass).removeClass("hide");  
                }        
            }
        }
        if(fromId == chatId || toId == chatId ){
            str+= ` <div class="message_container">`;
            let toName = $('#usernameval').val();
            if(data.type == 'personal'){
                if( messages.length>0 &&  messages[0].from_name !== toName ){
                    str += `<div class="alert alert-warning" role="alert"> 
                    Chat started by ${messages[0].from_name}. </div>`;
                }else{
                    str += `<div class="alert alert-warning" role="alert"> 
                    Chat started by ${toName}. </div>`;
                }
            }else{
                if( messages.length>0){
                    groupAdmin = messages[0].from_name;
                    str += `<div class="alert alert-warning" role="alert">
                    ${groupAdmin} created group '${messages[0].to_name}'. </div>`;
                    messages.splice(0, 1);
                }                
            }
            
            if(messages.length>0){
                messages.forEach(element=> {        
                    var d    = new Date(element.date);
                    var time = formatAMPM(d);
                    if(data.type  == 'personal'){
                        if( element.from_id == currentUserId){
                            // console.log('element.message = ',element.message );
                            str += `
                            <div class="container  float-right msg-div" >
                               
                                    <img src="images/user02.png" alt="Avatar" class="right" style="width:90%;">
                                
                                    <p class="message_view">
                                        ${element.message}
                                    </p>
                                    <div class="">
                                    <span class="sender time-left">You,&nbsp; </span> <span class="time-left"> ${time}</span>
                                    </div>
                            </div>`;
                        }else{
                            str += ` <div class="container darker float-left msg-div ">
                                            <div>
                                                <div class="float-left">
                                                    <img src="images/user01.png" alt="Avatar" style="width:90%; float: left; margin-right: 18px;">
                                                </div>
                                                <div class="float-left">
                                                    <p  class="message_view">${element.message}</p>
                                                </div>
                                                <div style="clear:both">
                                                </div>
                                            </div>
                                                <span class="time-left float-right"> ${time}</span>
                                                <span class="sender time-left float-right" >${element.from_name},&nbsp; </span> 
                                            
                                     </div>   `;
                        } 
                    }else{
                        if(element.group_type == 'add'){
                            str += `
                            <div class="container  msg-div add_member">
                                <p>   Admin added ${element.from_name}. </p>
                            </div> `;
                        }else if( element.group_type == 'remove'){
                            str += ` <div class="alert alert-warning" role="alert">
                           Admin removed ${element.from_name}. </div>
                            </div> `;
                        }else{
                            if( element.from_id == currentUserId){
                                str += `
                                <div class="container  float-right msg-div">
                                <img src="images/user02.png" alt="Avatar" class="right" style="width:90%;">
                                <p data-emoji-input="unicode">${element.message}</p>
                                <span class="sender time-left">You, &nbsp;</span> <span class="time-left"> ${time}</span>
                          
                                </div>`;
                            }else{
                                str += ` <div class="container darker float-left msg-div ">
                                <img src="images/user01.png" alt="Avatar" style="width:90%;float: left; margin-right: 18px;">
                                <p data-emoji-input="unicode"  style="float:left">${element.message}</p>
                                <span class="sender time-left">${element.from_name},&nbsp; </span> <span class="time-left"> ${time}</span>

                                </div>   `;
                            } 
                        }
                    }
                });
            }
            str += '</div>';             
            $('.messages_div').html(str);
        
            $('.messages_div').removeClass('hide');
        }
        setTimeout(
        function() 
        {
            $('.message_container').scrollTop(10000);     
        }, 10);
    });
    function formatAMPM(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var year = date.getYear();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }
    function sendMessage(){console.log('-------------------------------------[3]');
        let from    = $('#usernameId').val();
        let name    = $('#usernameval').val();
        let toName  = $('#usernameval').val();
        let type    = chatType;
        let to      = $('#to_id').val();
        if(type == 'personal'){
            let spanClass = 'span_message_'+to ;
            $('#'+spanClass).addClass("hide");   
        }else{
            let spanClass = 'span_message_group_'+to ;
            $('#'+spanClass).addClass("hide");     
        }
        // let message = $('#message').val().trim();
        let message = $('.emoji-wysiwyg-editor ').html();
        //  message = $('<textarea />').html(message).text();
   
        console.log('message = ',message );
        if(message){console.log('-------------------------------------[4]');
            let time  = new Date();
            socket.emit('sendMessage', {from:from,to:to,message:message,time:time,type:type});
            $('#message').val('');
            $('.emoji-wysiwyg-editor').html('');
        }
    }
    function getcurrentTime(){
        var d      = new Date();
        var time = d.getHours()+' : '+ d.getMinutes();
        return time;
    }    
    $('#registerbtn').on('click', function () {
       $('#registerDiv').removeClass('hide');
       $('#mainDiv').addClass('hide');
    });
    $('.sign_out').on('click', function () {
        sessionStorage.clear();
        socket.emit('disconnect');
        location.reload();
     });
    $('#loginbtn').on('click', function () {
        $('#loginDiv').removeClass('hide');
        $('#mainDiv').addClass('hide');
 
     });
     $('#loginpassword,#loginusername').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            login();
        }
    });
    $('#name,#password').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            setUsername();
        }
    });
    $('#message').on('input', () => {
        console.log('111111111' );
        updateTyping();
        
    });
    $('.msg-send').on('keydown', '.emoji-wysiwyg-editor', function (e)
    {
        var key = e.which;
        if (key == 13 && e.shiftKey){
          console.log('Key combination Enter+Shift');
          var divHeight = $('.emoji-wysiwyg-editor ').height(); 
          console.log('divHeight = ',divHeight );
          let increaseHeight = 30;
          $('.emoji-wysiwyg-editor').css('min-height', divHeight+increaseHeight+'px');
        }else if (key == 13){console.log('-------------------------------------[2]');
            $('.emoji-wysiwyg-editor').css('height', '34px');
            $('.emoji-wysiwyg-editor').css('min-height', '34px');
            $( "#sendMessage").trigger( "click" );
            e.preventDefault();
        }
    });
    


     socket.on("typing",(data)=>{
        $('.typing_div_'+data.fromId).removeClass('hide');
     });
     // Updates the typing event
     const updateTyping = () => {
         let userId = $('#usernameId').val();
         let toId   = $('#to_id').val();
        if (!typing) {
            typing = true;
            socket.emit('typing',{fromId:userId,toId:toId});
        }
        lastTypingTime = (new Date()).getTime();
        setTimeout(() => {
            var typingTimer = (new Date()).getTime();
            var timeDiff = typingTimer - lastTypingTime;
            if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
                socket.emit('stop typing',{fromId:userId,toId:toId});
                typing = false;
            }
        }, TYPING_TIMER_LENGTH);
    }
    socket.on("stop typing",(data)=>{
        $('.typing_div_'+data.fromId).addClass('hide');
     });
   
});
 
               